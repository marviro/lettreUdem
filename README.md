# lettreUdem

Un modèle pour écrire des lettres avec le logo dell'Université de Montréal.

Pour l'utiliser il faut d'abord installer pandoc et latex.

Écrire la lettre en markdown.

Ensuite lancer la commande

```
pandoc -f markdown -t latex --template=lettreudem.latex --pdf-engine=xelatex nomdufichier.md -o output.pdf
```
